
CREATE DATABASE fa_db DEFAULT CHARACTER SET utf8;

USE fa_db;

CREATE TABLE fa_data
(id int NOT NULL AUTO_INCREMENT,
title varchar(256) NOT NULL,
fadata_url text NOT NULL,
PRIMARY KEY(id));

CREATE TABLE user_data
(id int NOT NULL AUTO_INCREMENT,
login_id varchar(256) NOT NULL,
name varchar(256) NOT NULL,
password varchar(256) NOT NULL,
PRIMARY KEY(id));

CREATE TABLE keizibann
(id int NOT NULL AUTO_INCREMENT,
user_id int NOT NULL,
title varchar(256) NOT NULL,
info text NOT NULL,
create_data date NOT NULL,
PRIMARY KEY(id));

CREATE TABLE test_title
(id int NOT NULL AUTO_INCREMENT,
name varchar(256) NOT NULL,
PRIMARY KEY(id));

CREATE TABLE test_q
(id int NOT NULL AUTO_INCREMENT,
title_id int NOT NULL,
test_text varchar(256) NOT NULL,
a_1 int NOT NULL,
a_2 int,a_3 int,a_4 int,
test_pt int NOT NULL,
PRIMARY KEY(id));

CREATE TABLE a_text
(id int NOT NULL AUTO_INCREMENT,
test_q_id int NOT NULL,
a_1_text varchar(256) NOT NULL,
a_2_text varchar(256) NOT NULL,
a_3_text varchar(256) NOT NULL,
a_4_text varchar(256) NOT NULL,
PRIMARY KEY(id));

CREATE TABLE user_a
(id int NOT NULL AUTO_INCREMENT,
user_id int NOT NULL,
test_q_id int NOT NULL,
a_1 int NOT NULL,
a_2 int,a_3 int,a_4 int,
PRIMARY KEY(id));

CREATE TABLE test_data
(id int NOT NULL AUTO_INCREMENT,
user_id int NOT NULL,
title_id int NOT NULL,
test_allpt int NOT NULL,
PRIMARY KEY(id));

INSERT INTO fa_data(title,fadata_url) VALUES
('出血','https://docs.google.com/presentation/d/1IBWoryeHOTfGTZz-BxUl4viw4NwNb7gQ-Gbrx8yDuG4/edit?usp=sharing'),
('足攣り','https://docs.google.com/presentation/d/1kAU_zYGkxLAMx-p7D8kSax-o7FH3Tm1zr298zbrooek/edit?usp=sharing'),
('三角巾','https://docs.google.com/presentation/d/1i5wUseIuuhaN0g-MMtrigeKitr5U7QRlUAAT8sKJNfs/edit?usp=sharing'),
('熱中症の対策と対処','https://docs.google.com/presentation/d/1rak7Io38LkuHaKBQf0d5ldDG1sJtzEMRJwGQz9B1z1k/edit?usp=sharing');

INSERT INTO user_data(login_id,name,password) VALUES
('kannrisya','管理者','pass');

INSERT INTO test_title(name) VALUES
('ＣＰＲ（心肺蘇生法）'),('ＦＡ（初期手当）');

INSERT INTO test_q(title_id,test_text,a_1,a_2,a_3,a_4,test_pt) VALUES
('1','蘇生率が50％以下になるのは心肺停止後何分','2',null,null,null,'10'),
('1','圧迫するときの圧迫のの深さは何センチ','2',null,null,null,'10'),
('1','傷病者を発見した時最初にすることは次のうちどれ','4',null,null,null,'10'),
('1','ＡＥＤを持ってきた人がＡＥＤを使えなかった場合の対処は次のうちどれ。','1',null,null,null,'10'),
('1','ＣＰＲ中に傷病者が嘔吐した場合の対処はどれか','3',null,null,null,'10'),
('1','ＡＥＤ関する正しいものを選べ','1',null,null,null,'10'),
('1','ＣＰＲでもっとも大切だとされているのは次のうちどれ','4',null,null,null,'10'),
('1','嘔吐を除去し呼吸の回復が見られない場合の対処はどれ','1',null,null,null,'10'),
('1','119番通報後救急車が現場に到着する時間の平均は次のうちどれ（2019.12.）','3',null,null,null,'10'),
('1','ＣＰＲで必ずやらなくても良いことは次のうちどれ','1','4',null,null,'10'),
('2','鼻血が出た場合の適切な処置はどれ','3',null,null,null,'10'),
('2','足から多量出血している場合のもっとも適切な対処はどれか','4',null,null,null,'10'),
('2','足を攣った場合の対処はどれ','1',null,null,null,'10'),
('2','攣りやすくなる主な原因は次のうちどれ','1','3',null,null,'10'),
('2','三角巾で対応できない怪我は次のうちどれ','4',null,null,null,'10'),
('2','圧迫止血をすべて選びなさい','1','2','4',null,'10'),
('2','熱中症の種類を選びなさい','1',null,null,null,'10'),
('2','熱中症または塩分不足になったとき塩をなめるとどんな味がする？','3',null,null,null,'10'),
('2','熱中症（軽度）になったときの対処はどれ','2',null,null,null,'10'),
('2','熱中症になりやすいものはどれ','1','3','4',null,'10');

INSERT INTO a_text(test_q_id,a_1_text,a_2_text,a_3_text,a_4_text) VALUES
('1','6分','5分','4分','3分'),
('2','3cm','5cm','6cm','8cm'),
('3','119番通報','意識の確認','AEDを持ってくる','周囲の確認'),
('4','心臓マッサージの説明をし操作を変わってもらう','ＡＥＤもしくはＣＰＲができる人を呼んでもらう','ＡＥＤの使い方を説明する','心臓マッサージをやめ自分でＡＥＤを操作する'),
('5','ＣＰＲを続ける','そのままの体位で口内の嘔吐物を除去する','気道確保を解除し体を横向きにして嘔吐物を口内から除去する','気道確保を解除しＣＰＲを続ける'),
('6','電気を通すもの（金属・水分など）を傷病者から外す','幼児も大人も同じ電気量でも良い','ペースメーカーを付けているかを確認し、付けていたらAEDは使用できない','電気を流すときも圧迫は続ける'),
('7','人工呼吸','ＡＥＤ','周囲の確認','心臓マッサージ'),
('8','元の体位に戻しＣＰＲを再開する','そのままの体位で人工呼吸をする','元の体位に戻しＡＥＤを使用する','そのままの体位でＣＰＲを再開する'),
('9','10分以上','6〜7分','8〜9分','5分'),
('10','人工呼吸','意識の確認','心臓マッサージ','ＡＥＤ'),
('11','出血している方の鼻にティシュなどを詰める','若干上を向きながら鼻の軟骨と骨の境目を強く押さえる','若干下向きで鼻の軟骨と骨の境目を強く押さえる','止まるまでタオルなどで軽く押さえる'),
('12','血に触れると感染の恐れがあるため傷口を心臓より高いところに固定する','心臓より高いところで傷口を固定し、直接手で強く圧迫する','ハンカチなどで傷口を直接強く圧迫する','足を心臓より高いところに固定し、ハンカチなどで傷口を直接強く圧迫する'),
('13','攣っている部分を温めながら伸ばす','治るまで待つ','攣っている部分を冷やしながら伸ばす','湿布を貼る'),
('14','寝不足や疲労','運動不足','水分不足','暑さ'),
('15','骨折や脱臼などの固定','出血などの外傷','捻挫の固定','対応できない怪我は基本ない'),
('16','直接圧迫止血法','間接圧迫止血法','緊急圧迫止血法','緊縛止血法'),
('17','日射病・熱射病・熱痙攣・熱疲労','熱疲労・熱痙攣','日射病・熱射病','熱疲労'),
('18','塩辛い','酸っぱい','甘い','苦い'),
('19','気にせずそのまま活動を続ける','涼しい場所で水分補給をしながら安静にする','氷を張った水風呂に入る','救急車を呼ぶ'),
('20','過度な冷房が効いた室内と外の行き来','適度な運動','長時間の炎天下の下での活動','水分補給のみをする');
