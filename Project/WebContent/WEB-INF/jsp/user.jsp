<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>user</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
<div style="background-color:#00bfff;padding: 10px;border: 3px;">
    <div align="right">
        <font color="ffffff">ユーザ名さん　　</font>　

       <a href="UserData" class="navbar-link logout-link" style="text-decoration:none;">
            <i class="material-icons">
               supervised_user_circle　
            </i>
       </a>

       <a href="LogOut" class="navbar-link logout-link" style="text-decoration:none;">
           <i class="material-icons">
              input　
           </i>
       </a>
    </div>
</div>

    <h1><center><b>ユーザ詳細</b></center></h1>
<form class="form-signin" action="User" method="post">
<br>
<center>

    <p>ユーザID：gouda</p>

    <p>　ユーザ名：合田隼人</p>


    <br><a href="UpdataUser" class="btn btn-primary">更新</a>
<br>
<br><a href="TestData">テスト一覧</a>
</center>

<br><a href="FaReport" class="navbar-link logout-link">　戻る</a>


</form>
</body>

</html>