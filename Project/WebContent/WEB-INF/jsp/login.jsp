<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>login</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
<br>
    <h1><center><b>ログイン画面</b></center></h1>
<form class="form-signin" action="Login" method="post">
<br>
<br>
<br>
<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		 <font color="red">${errMsg}</font>
		</div>
</c:if>
    <center>
        <p><font face="BIZ UDゴシック">
            ログインID：
            </font>
            <input type="text" name="id" size="30" maxlength="30">
        </p>

        <p><font face="BIZ UDゴシック">
            パスワード：
            </font>
            <input type="password" name="pass" size="30" maxlength="30">
        </p>

    <br><a href="FaReport">
        <input type="submit"  value="ログイン" class="btn btn-primary">
        </a>
    <br><a href="NewUser" class="navbar-link logout-link">新規登録</a>
    </center>
</form>
</body>

</html>