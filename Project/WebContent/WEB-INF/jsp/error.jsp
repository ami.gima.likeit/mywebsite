<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>fareport</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
<br>
    <h1><center><font color="red"><b>システムエラー</b></font></center></h1>
<br>
<center>
<div class="container">
  <div class="col-12">
       <div class="alert alert-danger" role="alert">
          <font color="red">エラーメッセージ</font>
       </div>
  </div>
</div>
</center>

<a href="fareport.html">TOPページ</a>

</body>

</html>