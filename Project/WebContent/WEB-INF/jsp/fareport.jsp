<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>fareport</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>

<body>
<div style="background-color:#00bfff;padding: 10px;border: 3px;">
    <div align="right">
        <font color="ffffff">ユーザ名さん　</font>　

        <a href="User" class="navbar-link logout-link" style="text-decoration:none;">
            <i class="material-icons">
                account_circle　
            </i>
        </a>

        <a href="Kizibann" class="navbar-link logout-link" style="text-decoration:none;">
            掲示板　
        </a>

        <a href="LogOut" class="navbar-link logout-link">
           <i class="material-icons">
               input　
           </i>
        </a>
    </div>
</div>

    <h1><center><d>FA資料一覧画面</d></center></h1>
<form class="form-signin" action="FaReport" method="post">

<center>
<div class="CPR">
<h3>ＣＰＲ（心肺蘇生法）
    <a href="TestCPR" class="navbar-link logout-link" style="text-decoration:none;">
            <i class="material-icons">
                create
            </i>
    </a>
</h3>
    <a href="https://docs.google.com/presentation/d/1us9YVWWFf_t_gZ_uIeICvOAR1bpQnm12lzqRZqbApXY/edit?usp=sharing" style="text-decoration:none;">
       CPR（心肺蘇生法）
    </a>
<br><a href="https://docs.google.com/presentation/d/13iCblts3ABZYaiK8gXY8vvz8FU4ew14m1q7mMhVOnRE/edit?usp=sharing" style="text-decoration:none">
       AED（自動体外式除細動器）
    </a>
</div>

<div class="FA">
<br><h3>ＦＡ（初期手当）
    <a href="TestFa" class="navbar-link logout-link" style="text-decoration:none;">
            <i class="material-icons">
            create
            </i>
    </a>
    </h3>
<a href="https://docs.google.com/presentation/d/1IBWoryeHOTfGTZz-BxUl4viw4NwNb7gQ-Gbrx8yDuG4/edit?usp=sharing" style="text-decoration:none;">
    出血
</a>
<br><a href="https://docs.google.com/presentation/d/1kAU_zYGkxLAMx-p7D8kSax-o7FH3Tm1zr298zbrooek/edit?usp=sharing">
    足攣り
</a>
<br><a href="https://docs.google.com/presentation/d/1i5wUseIuuhaN0g-MMtrigeKitr5U7QRlUAAT8sKJNfs/edit?usp=sharing">
    三角巾
</a>
<br><a href="https://docs.google.com/presentation/d/1rak7Io38LkuHaKBQf0d5ldDG1sJtzEMRJwGQz9B1z1k/edit?usp=sharing">
    熱中症の対策と対処
</a>
</div>
</center>

</form>
</body>

</html>