<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>userdate</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<body>
<div style="background-color:#00bfff;padding: 10px;border: 3px;">
    <div align="right">
        <font color="ffffff">管理者さん　　　　</font>　
       <a href="LogOut" class="navbar-link logout-link">
           <i class="material-icons">
               input
           </i>
       </a>
    </div>
</div>

    <h1><center><b>ユーザ一覧</b></center></h1>
<form class="form-signin" action="UserData" method="post">
<center>
<table border="1">
  <thead>
    <tr bgcolor="#BBFFFF">
      <th scope="col">　ログインID　</th>
      <th scope="col">　ユーザ名　</th>
      <th scope="col"></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>gouda</td>
      <td>合田隼人</td>
　　　 <td>
       <a class="btn btn-danger" href="UserDelete">削除</a>
    　</td>
    </tr>
    <tr>
      <td>takahasi</td>
      <td>高橋陸</td>
　　　 <td>
       <a class="btn btn-danger" href="UserDelete">削除</a>
    　</td>
    </tr>
    <tr>
      <td>matida</td>
      <td>町田由美</td>
　　　 <td>
        <a class="btn btn-danger" href="UserDelete">削除</a>
    　</td>
    </tr>
  </tbody>
</table>
</center>

<br><a href="User" class="navbar-link logout-link">　戻る</a>


</form>
</body>

</html>