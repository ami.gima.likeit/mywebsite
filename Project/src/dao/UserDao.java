package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import model.UserDataModel;

public class UserDao {

	//ログイン処理
	public UserDataModel findByLoginInfo(String loginId, String password) {
        Connection conn = null;
        try {

            conn = DBManager.getConnection();

            String sql = "SELECT * FROM user_data WHERE login_id = ? and password = ?";

            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, password);
            ResultSet rs = pStmt.executeQuery();


            if (!rs.next()) {
                return null;
            }

            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new UserDataModel(loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

	public List<UserDataModel> findAll() {
        Connection conn = null;
        List<UserDataModel> userList = new ArrayList<UserDataModel>();

        try {

            conn = DBManager.getConnection();

            String sql = "SELECT * FROM user_data";

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                String password = rs.getString("password");
                UserDataModel user = new UserDataModel(id, loginId, name, password);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }

	//新規登録処理
	public int findByNewUserInfo(String loginId, String name,String password1,String password2) {
        Connection conn = null;
        int i=0;
	try{
		conn = DBManager.getConnection();

		String sql = "INSERT INTO user_data(login_id,name,password) VALUES(?,?,?)";

		PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, loginId);
        pStmt.setString(2, name);
        pStmt.setString(3, password1);
         i=pStmt.executeUpdate();
         System.out.println(i);


	}catch(SQLException e){
		e.printStackTrace();
	}finally{
		if(conn != null) {
			try {
				conn.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
	 }
	return i;
	}

	public UserDataModel findBynewUserInfo(String loginId) {
        Connection conn = null;
	try{
		conn = DBManager.getConnection();

		String sql = "SELECT * FROM user_data WHERE login_id = ?";

		PreparedStatement pStmt = conn.prepareStatement(sql);
        pStmt.setString(1, loginId);
        ResultSet rs = pStmt.executeQuery();

        if(!rs.next()) {
        	return null;
        }

        String loginIdData = rs.getString("login_id");
        return new UserDataModel(loginIdData);
	}catch(SQLException e){
		e.printStackTrace();
		return null;
	}finally{
		if(conn != null) {
			try {
				conn.close();
			}catch(SQLException e){
				e.printStackTrace();
				return null;
			}
		}
	 }
  }

	//暗号化
	public String findBySignaluserInfo(String Password) {
		 Charset charset = StandardCharsets.UTF_8;
			String algorithm = "MD5";
			String result = null;

			try {
			byte[] bytes = MessageDigest.getInstance(algorithm).digest(Password.getBytes(charset));
			result = DatatypeConverter.printHexBinary(bytes);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}

			return result;
	}

}
