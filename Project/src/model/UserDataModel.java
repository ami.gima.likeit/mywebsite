package model;

import java.io.Serializable;

public class UserDataModel implements Serializable{
	private int id;
	private String loginId;
	private String name;
	private String password;

		// ログインセッションを保存するためのコンストラクタ
	public UserDataModel(String loginId, String name) {
	    this.loginId = loginId;
		this.name = name;
	   }

	   // 全てのデータをセットするコンストラクタ
	public UserDataModel(int id, String loginId, String name, String password) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.password = password;

	}

	public UserDataModel(String loginId) {
		this.loginId = loginId;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
